import { __makeTemplateObject } from "tslib";
import { createGlobalStyle } from "styled-components";
import { makeFontFace } from "@heather-turano-coaching/design-system/utils";
export var FontFamily = createGlobalStyle(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  ", "\n"], ["\n  ", "\n"])), makeFontFace());
var templateObject_1;
//# sourceMappingURL=FontFamily.js.map